<?php
namespace Sl\Common;

use Sl\Helper\Lists\ManagerInterface;

trait HasListManager
{
    /**
     *
     * @var ManagerInterface
     */
    protected $listsManager;

    /**
     *
     * @param ManagerInterface $manager
     * @return HasListsManager
     */
    public function assignListsManager(ManagerInterface $manager)
    {
        $this->listsManager = $manager;
        return $this;
    }

    /**
     *
     * @return ManagerInterface
     * @throws \Exception
     */
    public function fetchListsManager()
    {
        if(!isset($this->listsManager)) {
            throw new \Exception('No list manager available');
        }
        return $this->listsManager;
    }
}