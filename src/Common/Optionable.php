<?php
namespace Sl\Common;

trait Optionable
{
    protected $options = array();
    
    public function hasOption($name)
    {
        return isset($this->options[$name]);
    }

    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
        return $this;
    }

    public function getOption($name, $default = null)
    {
        if($this->hasOption($name)) {
            return $this->options[$name];
        }
        return $default;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function cleanOption($name)
    {
        unset($this->options[$name]);
        return $this;
    }

    public function cleanOptions()
    {
        $this->options = array();
        return $this;
    }

    public function addOptions(array $options = array())
    {
        foreach($options as $name=>$value) {
            $this->setOption($name, $value);
        }
        return $this;
    }

    public function setOptions(array $options = array())
    {
        return $this->cleanOptions()->addOptions($options);
    }
}