<?php
namespace Sl\Model;

abstract class Basic implements ModelInterface
{
    use \Sl\Common\HasListManager;
    
    protected $id;
    protected $create;
    protected $archived;
    protected $active;
    protected $extend;

    protected $loged;

    protected $lists = array();

    protected $modelName;
    protected $moduleName;

    protected $related = array();
    protected $essentials = array();

    protected $controlField;

    protected $managers = array();

    public function getActive()
    {
        return $this->active;
    }

    public function getArchived()
    {
        return $this->archived;
    }

    public function getCreate()
    {
        return $this->create;
    }

    public function getExtend()
    {
        return $this->extend;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function setArchived($archived)
    {
        $this->archived = $archived;
        return $this;
    }

    public function setCreate($create)
    {
        $this->create = $create;
        return $this;
    }

    public function setExtend($extend)
    {
        $this->extend = $extend;
        return $this;
    }
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function assignRelated($relationName, array $related)
    {
        $relationName = $this->prepareRelationName($relationName);
        $this->related[$relationName] = array();
        
        foreach($related as $id=>$model) {
            $modelId = $id;

            if($model instanceof ModelInterface) {
                if($model->getId()) {
                    $modelId = $model->getId();
                }
            } elseif($model && !is_array($model)) {
                $modelId = $model;
            }
            $this->related[$relationName][$modelId] = $model;
        }
        
        return $this;
    }

    public function fetchRelated($relationName = null)
    {
        if(is_null($relationName)) {
            return $this->related;
        }
        
        $relationName = $this->prepareRelationName($relationName);
        if(isset($this->related[$relationName])) {
            return $this->related[$relationName];
        }
        
        return array();
    }

    public function fetchOneRelated($relationName)
    {
        $relationName = $this->prepareRelationName($relationName);
        if(!$this->issetRelated($relationName)) {
            return null;
        }
        $related = $this->fetchRelated($relationName);
        if(empty($related)) {
            return null;
        }
        reset($related);
        $first = current($related);
        if($first instanceof ModelInterface) {
            return $first;
        } else {
            return key($related);
        }
    }

    public function clearRelated($relationName = null)
    {
        if(!is_null($relationName)) {
            $relationName = $this->prepareRelationName($relationName);
            if(isset($this->related[$relationName])) {
                unset($this->related[$relationName]);
            }
        } else {
            $this->related = array();
        }
        return $this;
    }

    public function issetRelated($relationName)
    {
        $relationName = $this->prepareRelationName($relationName);
        return isset($this->related[$relationName]);
    }

    public function extract(array $fields = array())
    {
        $data = array();

        if(!count($fields)) {
            $reflection = new \ReflectionClass($this);
            foreach($reflection->getProperties(\ReflectionProperty::IS_PROTECTED) as $property) {
                /*@var $property \ReflectionProperty*/
                $getter = \Sl\Helper\Model::buildGetter($property->getName());

                if($reflection->hasMethod($getter)) {
                    $fields[] = $property->getName();
                }
            }
        }

        foreach($fields as $fieldName) {
            $getter = \Sl\Helper\Model::buildGetter($fieldName);

            if(method_exists($this, $getter)) {
                $data[$fieldName] = $this->$getter();
            } else {
                $data[$fieldName] = null;
            }
        }

        return $data;
    }

    public function fill(array $data)
    {
        foreach($data as $name=>$value) {
            if($name == \Sl\Relation\Manager::ESSENTIAL_OPTION_KEY) {
                $this->fillEssentials($value);
                continue;
            }
            $matches = array();
            if(preg_match('/^'.\Sl\Relation\Manager::RELATION_FIELD_PREFIX.'_([^_]+)((?:_?)(?:.*))$/', $name, $matches)) {
                if(!$matches[2]) {
                    $this->fillRelation($matches[1], $value);
                }
                continue;
            }

            $setter = \Sl\Helper\Model::buildSetter($name);
            if(method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
        return $this;
    }

    public function findControlField()
    {
        return isset($this->controlField)?$this->controlField:null;
    }

    public function findControlStatus()
    {
        $controlField = $this->findControlfield();
        if($controlField) {
            return $this->{\Sl\Helper\Model::buildGetter($controlField)}();
        }
        return null;
    }

    public function findFilledRelations()
    {
        return array_keys($this->related);
    }

    public function isEmpty()
    {
        $values = $this->extract();
        unset($values['active']);
        unset($values['create']);

        if(count(array_diff($values, array('')))) {
            return false;
        }
        
        $relations = $this->fetchRelated();
        foreach($relations as $relates) {
            if(count($relates)) {
                return false;
            }
        }

        return true;
    }
    
    public function listValue($field, $value = null)
    {
        if(is_null($value)) {
            $getter = \Sl\Helper\Model::buildGetter($field);
            if(method_exists($this, $getter)) {
                $value = $this->$getter();
            }
        }

        $listName = $this->listsArray($field);
        if($listName) {
            return $this->fetchListsManager()->getListValue($listName, $value);
        }
        return $value;
    }

    public function listsArray($property = null)
    {
        if(is_null($property)) {
            return $this->lists;
        } else {
            return isset($this->lists[$property])?$this->lists[$property]:null;
        }
    }

    public function assignEssentials($relationName, array $essentials)
    {
        $relationName = $this->prepareRelationName($relationName);
        $essentials = array_filter($essentials);

        $this->essentials[$relationName] = array();
        
        foreach($essentials as $id=>$model) {
            $modelId = $id;
            if($model instanceof ModelInterface) {
                if($model->getId()) {
                    $modelId = $model->getId();
                }
            } elseif(!is_array($model)) {
                $modelId = (int) $model;
            }
            $this->essentials[$relationName][$modelId] = $model;
        }
        return $this;
    }

    public function fetchEssentials($relationName, $onlyIds = false)
    {
        $relationName = $this->prepareRelationName($relationName);
        if(!$this->issetEssentials($relationName)) {
            return array();
        }
        if($onlyIds) {
            return array_keys($this->essentials[$relationName]);
        }
        return $this->essentials[$relationName];
    }

    public function issetEssentials($relationName)
    {
        $relationName = $this->prepareRelationName($relationName);
        return isset($this->essentials[$relationName]);
    }

    public function isLoged()
    {
        return (bool) $this->loged;
    }

    protected function fillRelation($relationName, $related)
    {
        $relationName = $this->prepareRelationName($relationName);

        if(!is_array($related)) {
            if(0 === strpos($related, ':')) {
                $related = array(trim($related));
            } else {
                $related = array_filter(array_unique(preg_split('/[^\d]/', $related)));
            }
        }

        return $this->assignRelated($relationName, $related);
    }

    protected function fillEssentials($essentials)
    {
        if(is_array($essentials)) {
            foreach($essentials as $name=>$values) {
                if(!is_array($values)) {
                    $values = array_filter(array_unique(preg_split('/[^\d]/', $values)));
                }
                $this->assignEssentials($name, $values);
            }
        }
        return $this;
    }

    protected function prepareRelationName($relationName)
    {
        return strtolower(trim((string) $relationName));
    }
}