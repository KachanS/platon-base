<?php
namespace Sl\Model;

interface NameableInterface extends ModelInterface
{
    public function setName($name);
    public function setDescription($description);

    public function getName();
    public function getDescription();
}