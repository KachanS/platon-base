<?php
namespace Sl\Model;

interface ModelInterface
{
    public function getId();
    public function getCreate();
    public function getActive();
    public function getArchived();
    public function getExtend();

    public function setId($id);
    public function setCreate($create);
    public function setActive($active);
    public function setArchived($archived);
    public function setExtend($extend);

    public function findModuleName();
    public function findModelName();

    public function isEmpty();
    public function isLoged();

    // Array transaltion
    public function fill(array $data);
    public function extract();

    // Relations
    public function assignRelated($relationName, array $related);
    public function fetchRelated();
    public function fetchOneRelated($relationName);
    public function clearRelated();
    public function issetRelated($relationName);
    public function findFilledRelations();

    // Essentials
    public function assignEssentials($relationName, array $essentials);
    public function fetchEssentials($relationName);
    public function issetEssentials($relationName);

    // Lists
    public function listsArray();
    public function listValue($field);

    // Statuses
    public function findControlStatus();
    public function findControlField();
}