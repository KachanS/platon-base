<?php
namespace Sl\Logger;

use Psr\Log\AbstractLogger;

class FileLogger extends AbstractLogger
{
    protected $filePath;
    
    public function __construct($filePath)
    {
        
        $this->touchFile($filePath);

        if(!is_writable($filePath)) {
            throw new \Exception('File "'.$filePath.'" isn\'t writable');
        }

        $this->filePath = $filePath;
    }

    protected function touchFile($filePath) {
        
        if(!is_file($filePath)) {
            
            try {
                $file = fopen($filePath,"w");
                fwrite($file, '');
                fclose($file);
            } catch (\Exception $e) {
                
            }
        }
        
        if(!is_file($filePath)) {
            throw new \Exception('Given path "'.$filePath.'" doesn\'t exists');
        }
    }


    public function log($level, $message, array $context = array()): void
    {
        $context['logLevel'] = '['.strtoupper($level).']';

        $messageString = is_string($message)?$message:print_r($message, true);
        $messageString = '%logLevel%: '.$messageString;

        $keys = $values = [];
        foreach($context as $k=>$v) {
            $keys[] = '%'.$k.'%';
            $values[] = $v;
        }

        file_put_contents($this->filePath, date('y/m/d H:i:s').' '. str_replace($keys, $values, $messageString.PHP_EOL), FILE_APPEND);
    }
}