<?php
namespace Sl\EventManager;

use Sl\EventManager\Event\FactoryInterface;

class Simple implements ManagerInterface, Event\FactoryAwareInterface
{
    use Event\FactoryAwareTrait;
    /**
     *
     * @var FactoryInterface
     */
    protected $eventFactory;

    protected $handlers = [];

    const PRIORITY_HIGH = 1;
    const PRIORITY_NORMAL = 2;
    const PRIORITY_LOW = 3;

    const NAMESPACE_SEPARATOR = '.';
    const WILDCARD = '*';
    
    public function on($eventName, $handler, $priority = null): ManagerInterface
    {
        if(!$eventName) {
            throw new \Exception('"eventName" param is required');
        }

        if(!is_callable($handler)) {
            throw new \Exception('"handler" should be valid callable. '.(is_object($handler)?get_class($handler):gettype($handler)).' given');
        }

        if(!isset($this->handlers[$eventName])) {
            $this->handlers[$eventName] = [
                self::PRIORITY_HIGH => [],
                self::PRIORITY_NORMAL => [],
                self::PRIORITY_LOW => [],
            ];
        }

        if(is_null($priority)) {
            $priority = self::PRIORITY_NORMAL;
        }

        if(!in_array($priority, $this->getAvailablePriorities())) {
            throw new \Exception('Invalid priority level');
        }

        $this->handlers[$eventName][$priority][] = $handler;

        return $this;
    }

    public function trigger($eventName, array $data = array()): EventInterface
    {
        $eventName = trim($eventName, self::NAMESPACE_SEPARATOR);
        
        if(false !== strpos($eventName, self::WILDCARD)) {
            throw new \Exception('Event name can\'t include wildcard character "'.self::WILDCARD.'"');
        }

        $event = $this->eventFactory->build($eventName, $data);

        $eventNames = [$eventName];
        $handlersByPriority = [];

        $separated = explode(self::NAMESPACE_SEPARATOR, $eventName);
        while(count($separated) > 1) {
            array_pop($separated);
            $eventNames[] = implode(self::NAMESPACE_SEPARATOR, $separated).self::NAMESPACE_SEPARATOR.self::WILDCARD;
        }

        foreach($eventNames as $name) {
            $handlersByPriority = \Sl\Helper\Generic::merge($handlersByPriority, $this->handlers[$name] ?? []);
        }
        
        $priorities = [self::PRIORITY_HIGH, self::PRIORITY_NORMAL, self::PRIORITY_LOW];

        foreach($priorities as $priority) {
            $handlers = $handlersByPriority[$priority] ?? [];

            foreach($handlers as $handler) {
                $event = $this->invokeHandler($handler, $event);

                if($event->isPropagationStopped()) {
                    break 2;
                }
            }
        }

        return $event;
    }

    protected function invokeHandler($handler, EventInterface $event)
    {
        if(!is_callable($handler)) {
            throw new \Exception('Handler should be callable');
        }
        
        $result = call_user_func($handler, $event);

        if(!($result instanceof EventInterface)) {
            $result = $event;
        }

        return $event;
    }

    protected function getAvailablePriorities()
    {
        return [
            self::PRIORITY_HIGH,
            self::PRIORITY_NORMAL,
            self::PRIORITY_LOW,
        ];
    }
}