<?php
namespace Sl\EventManager\Event;

use Sl\EventManager\EventInterface;

class SimpleFactory implements FactoryInterface
{
    public function build(string $name, array $options = []): EventInterface
    {
        return new Simple($name, $options);
    }
}