<?php
namespace Sl\EventManager\Event;

interface FactoryAwareInterface
{
    public function setEventFactory(FactoryInterface $factory);
    public function getEventFactory(): FactoryInterface;
}