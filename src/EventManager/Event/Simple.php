<?php
namespace Sl\EventManager\Event;

use Sl\EventManager\EventInterface;

class Simple implements EventInterface
{
    use \Sl\Common\Optionable;
    
    protected $name;
    protected $propagationStopped = false;
    
    public function __construct($name, array $options = [])
    {
        $this->name = (string) $name;
        
        $this->setOptions($options);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isPropagationStopped(): bool
    {
        return (bool) $this->propagationStopped;
    }

    public function stopPropagation(): EventInterface
    {
        $this->propagationStopped = true;
        return $this;
    }
}