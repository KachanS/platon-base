<?php
namespace Sl\EventManager\Event;

use Sl\EventManager\EventInterface;

interface FactoryInterface
{
    public function build(string $name, array $build = []): EventInterface;
}