<?php
namespace Sl\EventManager\Event;

trait FactoryAwareTrait
{
    protected $eventFactory;

    /**
     *
     * @return FactoryInterface
     */
    public function getEventFactory(): FactoryInterface
    {
        return $this->eventFactory;
    }

    /**
     *
     * @param FactoryInterface $factory
     * @return $this
     */
    public function setEventFactory(FactoryInterface $factory)
    {
        $this->eventFactory = $factory;
        return $this;
    }
}