<?php
namespace Sl\EventManager;

interface ManagerInterface
{
    public function on($eventName, $handler, $priority = null):ManagerInterface;
    public function trigger($eventName, array $data = []):EventInterface;
}