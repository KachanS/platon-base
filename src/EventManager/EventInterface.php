<?php
namespace Sl\EventManager;

interface EventInterface
{
    public function getName(): string;
    public function isPropagationStopped(): bool;
    public function stopPropagation(): EventInterface;
}