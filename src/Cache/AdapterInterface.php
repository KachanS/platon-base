<?php
namespace Sl\Cache;

interface AdapterInterface
{
    public function load($cacheId);
    public function save($data, $cacheId);
    public function test($cacheId);
    public function clean();
}