<?php
namespace Sl\Helper\Config;

interface WriterInterface
{
    public function write($filePath);
}