<?php
namespace Sl\Helper\Lists\Manager;

class Simple implements \Sl\Helper\Lists\ManagerInterface
{
    const STATUS_FINAL = 'final';
    const STATUS_CANCELED = 'canceled';
    const STATUS_PROCESSED = 'processed';

    protected $lists = [];
    protected $lazyLists = [];
    protected $statuses = [];

    public function getList($name)
    {
        if(isset($this->lazyLists[$name])) {
            $list = call_user_func($this->lazyLists[$name]);
            if(!is_array($list)) {
                throw new \Exception('Invalid list builder result');
            }
            unset($this->lazyLists[$name]);
            $this->registerList($name, $list);
        }
        
        if(isset($this->lists[$name])) {
            return $this->lists[$name];
        }
        return null;
    }

    public function getListStatuses($listName)
    {
        if(!isset($this->statuses[$listName])) {
            return null;
        }
        return $this->statuses[$listName]?$this->statuses[$listName]:array();
    }

    public function getListValue($listName, $key, $default = null)
    {
        $list = $this->getList($listName);
        if($list) {
            return isset($list[$key])?$list[$key]:$default;
        }
        return $default;
    }

    public function registerList($name, array $data)
    {
        $this->lists[$name] = $data;
        return $this;
    }

    public function registerLazyList($name, callable $listBuilder)
    {
        $this->lazyLists[$name] = $listBuilder;
        unset($this->lists[$name]);
        return $this;
    }

    public function registerListStatus($listName, $statusName, $values)
    {
        if(!$this->isStatusValid($statusName)) {
            return $this;
        }
        if(!isset($this->statuses[$listName])) {
            $this->statuses[$listName] = array();
        }
        if(!isset($this->statuses[$listName][$statusName])) {
            $this->statuses[$listName][$statusName] = array();
        }
        $this->statuses[$listName][$statusName] = array_merge($this->statuses[$listName][$statusName], $values);
        return $this;
    }

    public function registerListStatuses(array $statusData)
    {
        foreach($statusData as $statusName=>$lists) {
            if(!is_array($lists)) {
                continue;
            }
            foreach($lists as $listName=>$values) {
                $this->registerListStatus($listName, $statusName, $values);
            }
        }
        return $this;
    }

    public function checkValueIsStatusValue($listName, $value, $statusName)
    {
        $statuses = $this->getListStatuses($listName);
        if(isset($statuses[$statusName])) {
            return in_array($value, $statuses[$statusName]);
        }
        return false;
    }

    protected function isStatusValid($status)
    {
        return in_array($status, array(
            self::STATUS_FINAL,
            self::STATUS_CANCELED,
            self::STATUS_PROCESSED,
        ));
    }
}