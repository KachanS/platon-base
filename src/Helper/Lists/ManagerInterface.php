<?php
namespace Sl\Helper\Lists;

interface ManagerInterface
{
    public function getListValue($listName, $key);

    public function registerList($name, array $data);
    public function getList($name);

    public function registerListStatus($listName, $statusName, $values);
    public function registerListStatuses(array $statusData);
    public function getListStatuses($statusName);
    
    public function checkValueIsStatusValue($listName, $value, $statusName);
}