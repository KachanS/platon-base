<?php
namespace Sl\Helper\Model\Config\Reader;

use Sl\Helper\Generic;
use Sl\Helper\Model\Config\ReaderInterface;

class Simple implements ReaderInterface
{
    protected $data = array();

    const EXTERNALS_DIRNAME = 'externals';

    const ROLES_KEY = '_roles_';
    const ALLROLES_KEY = '_all_';

    const EC_DIR = 20001;
    const EC_MERGE = 20002;

    public function __construct(array $configDirs = array())
    {
        $configData = array(
            self::ROLES_KEY => array(),
        );

        foreach($configDirs as $moduleName=>$configsDir) {
            $configData[$moduleName] = array();
            
            try {
                if(is_dir($configsDir)) {
                    $cdh = opendir($configsDir);
                    if($cdh) {
                        while(false !== ($configName = readdir($cdh))) {
                            if($this->isDot($configName)) continue;
                            $configFilePath = implode(DIRECTORY_SEPARATOR, array($configsDir, $configName));

                            if(is_file($configFilePath)) {
                                $configData[$moduleName][pathinfo($configFilePath, PATHINFO_FILENAME)] = require $configFilePath;
                            } elseif(is_dir($configFilePath)) {
                                if($configName == self::EXTERNALS_DIRNAME) { // External module configuration
                                    $edh = opendir($configFilePath);
                                    if($edh) {
                                        while(false !== ($extModuleFile = readdir($edh))) {
                                            if($this->isDot($extModuleFile)) continue;

                                            $extModuleConfigDir = implode(DIRECTORY_SEPARATOR, array($configFilePath, $extModuleFile));
                                            if(is_dir($extModuleConfigDir)) {
                                                $emdh = opendir($extModuleConfigDir);
                                                if($emdh) {
                                                    while(false !== ($extConfigRole = readdir($emdh))) {
                                                        if($this->isDot($extConfigRole)) continue;

                                                        if(!isset($configData[self::ROLES_KEY][$extConfigRole])) {
                                                            $configData[self::ROLES_KEY][$extConfigRole] = array();
                                                        }

                                                        $extConfigRoleDir = implode(DIRECTORY_SEPARATOR, array($extModuleConfigDir, $extConfigRole));
                                                        if(is_dir($extConfigRoleDir)) {
                                                            $ecdh = opendir($extConfigRoleDir);
                                                            if($ecdh) {
                                                                while(false !== ($extModelConfigName = readdir($ecdh))) {
                                                                    if($this->isDot($extModelConfigName)) continue;

                                                                    $extModelConfigFilename = implode(DIRECTORY_SEPARATOR, array($extConfigRoleDir, $extModelConfigName));
                                                                    if(is_file($extModelConfigFilename)) {
                                                                        $curKey = pathinfo($extModelConfigFilename, PATHINFO_FILENAME);
                                                                        $currentData = @$configData[self::ROLES_KEY][$extConfigRole][$extModuleFile][$curKey];
                                                                        
                                                                        $newData = require $extModelConfigFilename;

                                                                        if(!$currentData) {
                                                                            $configData[self::ROLES_KEY][$extConfigRole][$extModuleFile][$curKey] = $newData;
                                                                        } else {
                                                                            $intersectedKeys = array_keys(array_intersect_key($currentData, $newData));
                                                                            if(count($intersectedKeys)) {
                                                                                throw new \Exception('There are some conflicts when building config for "'.$extModuleFile.':'.$curKey.'". See "'.$extModelConfigFilename.'" file. Intersected keys: '.implode(', ', $intersectedKeys).'', self::EC_MERGE);
                                                                            }
                                                                            $configData[self::ROLES_KEY][$extConfigRole][$extModuleFile][$curKey] = Generic::merge($currentData, $newData);
                                                                        }
                                                                    } elseif(is_dir($extModelConfigFilename)) {
                                                                        $ecmdh = opendir($extModelConfigFilename);
                                                                        if($ecmdh) {
                                                                            while(false !== ($extModelSubconfigName = readdir($ecmdh))) {
                                                                                if($this->isDot($extModelSubconfigName)) continue;

                                                                                $extModelSubconfigFilename = implode(DIRECTORY_SEPARATOR, array($extModelConfigFilename, $extModelSubconfigName));
                                                                                if(is_file($extModelSubconfigFilename)) {
                                                                                    $curKey = pathinfo($extModelSubconfigFilename, PATHINFO_FILENAME);
                                                                                    $currentData = @$configData[self::ROLES_KEY][$extConfigRole][$extModuleFile][$extModuleFile.'.'.$extModelConfigName][$curKey];
                                                                                    $newData = require $extModelSubconfigFilename;

                                                                                    if(!$currentData) {
                                                                                        $configData[self::ROLES_KEY][$extConfigRole][$extModuleFile][$extModuleFile.'.'.$extModelConfigName][$curKey] = $newData;
                                                                                    } else {
                                                                                        $intersectedKeys = array_keys(array_intersect_key($currentData, $newData));
                                                                                        if(count($intersectedKeys)) {
                                                                                            throw new \Exception('There are some conflicts when building config for "'.$extModuleFile.'.'.$extModelConfigName.':'.$curKey.'". See "'.$extModelSubconfigFilename.'" file. Intersected keys: '.implode(', ', $intersectedKeys).'', self::EC_MERGE);
                                                                                        }
                                                                                        $configData[self::ROLES_KEY][$extConfigRole][$extModuleFile][$extModuleFile.'.'.$extModelConfigName][$curKey] = Generic::merge($currentData, $newData);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        closedir($ecmdh);
                                                                    }
                                                                }
                                                            }
                                                            closedir($ecdh);
                                                        }
                                                    }
                                                } else {
                                                    throw new \Exception('Can not open directory "'.$extModuleConfigDir.'"', self::EC_DIR);
                                                }
                                                closedir($emdh);
                                            }
                                        }
                                    } else {
                                        throw new \Exception('Can not open directory "'.$configFilePath.'"', self::EC_DIR);
                                    }
                                    closedir($edh);
                                } else { // Model configs
                                    $alias = implode('.', array($moduleName, $configName));
                                    
                                    $modelDir = implode(DIRECTORY_SEPARATOR, array($configsDir, $configName));
                                    if(is_dir($modelDir)) {
                                        $mdh = opendir($modelDir);
                                        if($mdh) {
                                            while(false !== ($modelConfigFile = readdir($mdh))) {
                                                if($this->isDot($modelConfigFile)) continue;

                                                $modelConfigFilePath = implode(DIRECTORY_SEPARATOR, array($modelDir, $modelConfigFile));

                                                if(is_file($modelConfigFilePath)) {
                                                    $configData[$moduleName][$alias][pathinfo($modelConfigFilePath, PATHINFO_FILENAME)] = require $modelConfigFilePath;
                                                }
                                            }
                                        } else {
                                            throw new \Exception('Can not open directory "'.$modelDir.'"', self::EC_DIR);
                                        }
                                        closedir($mdh);
                                    } else {
                                        throw new \Exception('"'.$modelDir.'" is not model directory', self::EC_DIR);
                                    }
                                }
                            }
                        }
                    } else {
                        throw new \Exception('Can not open dir "'.$configsDir.'"', self::EC_DIR);
                    }
                    closedir($cdh);
                } else {
                    throw new \Exception('No "_config" dir found', self::EC_DIR);
                }
            } catch(\Exception $e) {
                switch($e->getCode()) {
                    case self::EC_DIR:
                    case self::EC_MERGE:
                        break;
                    default:
                        throw $e;
                }
            }
        }
        
        if(isset($configData[self::ROLES_KEY][self::ALLROLES_KEY])) {
            // Перерабатываем настройки для всех ролей (self::ALLROLES_KEY) ...
            foreach($configData[self::ROLES_KEY][self::ALLROLES_KEY] as $moduleName=>$moduleData) {
                foreach($moduleData as $alias=>$data) {
                    $aliasData = explode('.', $alias);

                    if(count($aliasData) > 1) {
                        $configData[$moduleName][$alias] = Generic::merge($configData[$aliasData[0]][$alias], $data);
                    } else {
                        $configData[$moduleName][$alias] = Generic::merge($configData[$moduleName][$alias], $data);
                    }
                }
            }
            // И удаляем их
            unset($configData[self::ROLES_KEY][self::ALLROLES_KEY]);
        }

        $this->data = $configData;
    }

    protected function isDot($name)
    {
        return in_array($name, array('.', '..'));
    }

    public function read(\Sl\Model\ModelInterface $model, $key)
    {
        $alias = \Sl\Helper\Model::getAlias($model);

        if(!isset($this->data[$model->findModuleName()])) {
            return null;
        }

        if($alias === $model->findModuleName()) {
            if(!isset($this->data[$model->findModuleName()][$key])) {
                return null;
            }
            
            return $this->data[$model->findModuleName()][$key];
        } else {
            if(!isset($this->data[$model->findModuleName()][$alias])) {
                return null;
            }

            if(!isset($this->data[$model->findModuleName()][$alias][$key])) {
                return null;
            }

            return $this->data[$model->findModuleName()][$alias][$key];
        }
    }
}