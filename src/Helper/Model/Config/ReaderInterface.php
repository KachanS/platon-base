<?php
namespace Sl\Helper\Model\Config;

use Sl\Model\ModelInterface;

interface ReaderInterface
{
    public function read(ModelInterface $model, $key);
}