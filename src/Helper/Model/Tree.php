<?php
namespace Sl\Helper\Model;

use Sl\Model\TreeModelInterface;

class Tree
{
    public static function fillRootNode(TreeModelInterface $model)
    {
        $model->setLeftKey(1);
        $model->setRightKey(2);
        $model->setLevel(1);

        return $model;
    }

    public static function isRoot(TreeModelInterface $model)
    {
        return ($model->getLeftKey() == 1) && ($model->getLevel() == 1);
    }
}