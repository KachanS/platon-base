<?php
namespace Sl\EventManager;

class SimpleTest extends \PHPUnit\Framework\TestCase
{
    protected $em;

    protected $handlers = [];

    protected function setUp()
    {
        $this->em = (new Simple())
                        ->setEventFactory(new Event\SimpleFactory());
    }

    /**
     * @dataProvider onCaseProvider
     */
    public function testOn($name, $callback, $expectsException)
    {
        if($expectsException) {
            $this->expectException(\Exception::class);
        }

        $this->assertInstanceOf(ManagerInterface::class, $this->em->on($name, $callback));
    }

    public function testTrigger()
    {
        // Should be never invoked
        $noneMock = $this->getMockBuilder(HandlerTest::class)->getMock();
        $noneMock->expects($this->never())->method('handle');

        // Should be invoked once
        $onceMock = $this->getMockBuilder(HandlerTest::class)->getMock();
        $onceMock->expects($this->once())->method('handle');

        // Should be invoked twice
        $twiceMock = $this->getMockBuilder(HandlerTest::class)->getMock();
        $twiceMock->expects($this->at(0))->method('handle')->willReturnArgument(0);
        $twiceMock->expects($this->at(1))->method('handle')->willReturnArgument(0);

        $this->em->on('test', [$onceMock, 'handle']);
        $this->em->on('test', [$twiceMock, 'handle']);
        $this->em->on('test2', [$twiceMock, 'handle']);
        $this->em->on('test3', [$noneMock, 'handle']);

        $this->assertInstanceOf(EventInterface::class, $this->em->trigger('test'));
        $this->assertInstanceOf(EventInterface::class, $this->em->trigger('test2'));

        $event = $this->em->trigger('.test4.');
        $this->assertInstanceOf(EventInterface::class, $event);
        $this->assertEquals('test4', $event->getName());
    }

    public function testHandlersPriority()
    {
        $event = $this->em->trigger('test', ['option' => 'value to override']);
        $this->assertEquals('value to override', $event->getOption('option'));

        $this->em->on('test2', $this->getHandler('option', 'value'));
        $event = $this->em->trigger('test2', ['option' => 'value to override']);
        $this->assertEquals('value', $event->getOption('option'));

        // All handler works
        $this->em->on('test3', $this->getHandler('option_h', 'value_high'), Simple::PRIORITY_HIGH);
        $this->em->on('test3', $this->getHandler('option_n', 'value_normal'), Simple::PRIORITY_NORMAL);
        $this->em->on('test3', $this->getHandler('option_l', 'value_low'), Simple::PRIORITY_LOW);
        $event = $this->em->trigger('test3', ['option' => 'value to override']);
        $this->assertEquals([
            'option' => 'value to override',
            'option_h' => 'value_high',
            'option_n' => 'value_normal',
            'option_l' => 'value_low',
        ], $event->getOptions());

        // Lower priority should run last
        $this->em->on('test4', $this->getHandler('option', 'value_normal_before'), Simple::PRIORITY_NORMAL);
        $this->em->on('test4', $this->getHandler('option', 'value_low'), Simple::PRIORITY_LOW);
        $this->em->on('test4', $this->getHandler('option', 'value_normal_after'), Simple::PRIORITY_NORMAL);
        $event = $this->em->trigger('test4', ['option' => 'value to override']);
        $this->assertEquals('value_low', $event->getOption('option'));

        // Lower priority should run last
        $this->em->on('test5', $this->getHandler('option', 'value_high_before'), Simple::PRIORITY_HIGH);
        $this->em->on('test5', $this->getHandler('option', 'value_normal'), Simple::PRIORITY_NORMAL);
        $this->em->on('test5', $this->getHandler('option', 'value_high_after'), Simple::PRIORITY_HIGH);
        $event = $this->em->trigger('test5', ['option' => 'value to override']);
        $this->assertEquals('value_normal', $event->getOption('option'));
    }

    public function testEventNamespaces()
    {
        $this->expectException(\Exception::class);
        $this->em->trigger('test.*');

        $this->em->on('test.*', $this->getHandler('option', 'value'));

        $event = $this->em->trigger('test', []);
        $this->assertEmpty($event->getOptions());

        $event = $this->em->trigger('test.action', []);
        $this->assertEquals('value', $event->getOption('option'));

        $event = $this->em->trigger('test.action.subaction', []);
        $this->assertEquals('value', $event->getOption('option'));

        $event = $this->em->trigger('ns.test', []);
        $this->assertEmpty($event->getOptions());
        
        $event = $this->em->trigger('ns.test.action', []);
        $this->assertEmpty($event->getOptions());
    }

    public function onCaseProvider()
    {
        return [
            // Invalid
            ['', function(){}, true],
            ['test', false, true],
            ['test', null, true],
            ['test', [], true],
            ['test', new \stdClass(), true],
            ['test', [new HandlerTest(), 'invalidMethod'], true],
            ['test', [HandlerTest::class, 'invalidStatic'], true],
            // Valid
            ['test', function(){}, false],
            ['test', [HandlerTest::class, 'staticHandler'], false],
            ['test', [new HandlerTest(), 'handle'], false],
            ['test', new HandlerTest(), false],
        ];
    }

    protected function getHandler($key, $value)
    {
        return function(Event\Simple $event) use ($key, $value) {
            $event->setOption($key, $value);
            return $event;
        };
    }
}

// Helper class to check callable logic
class HandlerTest
{
    public function handle($e)
    {
        return $e;
    }

    public static function staticHandler($e)
    {
        return $e;
    }

    public function __invoke($e)
    {
        return $e;
    }
}