<?php
namespace Sl\EventManager\Event;

class SimpleFactoryTest extends \PHPUnit\Framework\TestCase
{
    public function testBuild()
    {
        $factory = new SimpleFactory();

        $this->assertInstanceOf(Simple::class, $factory->build('event.name'));
    }
}