<?php
namespace Sl\EventManager\Event;

class SimpleTest extends \PHPUnit\Framework\TestCase
{
    public function testGetName()
    {
        $eventName = 'simple.event';
        
        $event = new Simple('simple.event');

        $this->assertEquals($eventName, $event->getName());
    }

    public function testPropagation()
    {
        $event = new Simple('simple.event');

        $this->assertFalse($event->isPropagationStopped());

        $this->assertInstanceOf(Simple::class, $event->stopPropagation());

        $this->assertTrue($event->isPropagationStopped());
    }
}