<?php
namespace Sl\Model;

abstract class TreeTest extends BasicTest
{
    /**
     *
     * @var Tree
     */
    protected $model;

    protected $modelName = 'base';
    protected $moduleName = 'main';
    
    public function setUp()
    {
        $this->model = $this->getModelMock($this->modelName, $this->moduleName);
    }

    public function simpleDataProvider()
    {
        $data = parent::simpleDataProvider();

        $data[] = array('left_key', rand(0, 10));
        $data[] = array('right_key', rand(0, 10));
        $data[] = array('level', rand(0, 10));

        return $data;
    }

    protected function getModelMock($modelName, $moduleName)
    {
        $model = $this->getMockForAbstractClass(Tree::class);

        $model->expects($this->any())->method('findModelName')->will($this->returnValue($modelName));
        $model->expects($this->any())->method('findModuleName')->will($this->returnValue($moduleName));

        return $model;
    }
}