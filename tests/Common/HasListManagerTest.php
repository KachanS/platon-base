<?php
namespace Sl\Common;

use Sl\Helper\Lists\ManagerInterface;

class HasListManagerTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     * @var HasListManager
     */
    protected $managable;

    /**
     *
     * @var ManagerInterface
     */
    protected $listsManager;
    
    public function setUp()
    {
        $this->managable = $this->getMockForTrait(HasListManager::class);
        $this->listsManager = $this->getMockForAbstractClass(ManagerInterface::class);
    }
    
    public function testAssignListsManager()
    {
        $this->assertAttributeEmpty('listsManager', $this->managable);

        $this->managable->assignListsManager($this->listsManager);

        $this->assertAttributeEquals($this->listsManager, 'listsManager', $this->managable);
    }
    
    public function testFetchListsManager()
    {
        $this->expectException(\Exception::class);
        $this->managable->fetchListsManager();
        
        $this->managable->assignListsManager($this->listsManager);
        $this->assertInstanceOf(ManagerInterface::class, $this->managable->fetchListsManager());
    }
}