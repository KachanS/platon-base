<?php
namespace Sl\Logger;

use org\bovigo\vfs\vfsStream;

class FileLoggerTest extends \PHPUnit\Framework\TestCase
{
    protected $root;

    const ROOT_PATH = 'root';

    const VALID_LOG_PATH = self::ROOT_PATH.'/logs/tests.log';
    const INVALID_FILE_PATH = self::ROOT_PATH.'/logs/test_invalid.log';
    const NOT_WRITABLE_FILE_PATH = self::ROOT_PATH.'/logs/test_nw.log';

    public function setUp()
    {
        $this->root = vfsStream::setup(self::ROOT_PATH, 0777, [
            'logs' => [
                'tests.log' => '',
            ]
        ]);

        vfsStream::newFile('logs/test_nw.log', 0444)->at($this->root)->setContent('');
    }

    public function testFileCreation()
    {
        $this->assertFileNotExists(vfsStream::url(self::INVALID_FILE_PATH));
        $logger = new FileLogger(vfsStream::url(self::INVALID_FILE_PATH));
        $this->assertFileExists(vfsStream::url(self::INVALID_FILE_PATH));
    }

    public function testNotWritableFile()
    {
        $this->assertFileExists(vfsStream::url(self::NOT_WRITABLE_FILE_PATH));
        $this->assertFileNotIsWritable(vfsStream::url(self::NOT_WRITABLE_FILE_PATH));
        
        $this->expectException(\Exception::class);
        $this->expectExceptionCode(0);

        new FileLogger(vfsStream::url(self::NOT_WRITABLE_FILE_PATH));
    }

    public function testLog()
    {
        $this->assertFileExists(vfsStream::url(self::VALID_LOG_PATH));
        $this->assertFileIsWritable(vfsStream::url(self::VALID_LOG_PATH));

        $this->assertEquals('', file_get_contents(vfsStream::url(self::VALID_LOG_PATH)));

        $logger = new FileLogger(vfsStream::url(self::VALID_LOG_PATH));
        $this->assertEquals('', file_get_contents(vfsStream::url(self::VALID_LOG_PATH)));

        $message = 'Some message';
        $logger->log(\Psr\Log\LogLevel::INFO, $message);
        $this->assertTrue(false !== strpos(file_get_contents(vfsStream::url(self::VALID_LOG_PATH)), $message));

        $secMessage = 'Second message';
        $logger->log(\Psr\Log\LogLevel::INFO, $secMessage);
        $this->assertTrue(false !== strpos(file_get_contents(vfsStream::url(self::VALID_LOG_PATH)), $message));
        $this->assertTrue(false !== strpos(file_get_contents(vfsStream::url(self::VALID_LOG_PATH)), $secMessage));
    }
}